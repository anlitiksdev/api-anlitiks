# API ANLITIKS

Rapid Analyzer Anlitiks Module

## Maven Commands 

## RUN Commands

LOCAL: mvn spring-boot:run -Dspring-boot.run.profiles=local
DEV: mvn spring-boot:run -Dspring-boot.run.profiles=dev
QA: mvn spring-boot:run -Dspring-boot.run.profiles=qa
PROD: mvn spring-boot:run -Dspring-boot.run.profiles=prod

## BUILD Commands

LOCAL: mvn clean install -Dspring.profiles.active=local
DEV: mvn clean install -Dspring.profiles.active=dev
QA: mvn clean install -Dspring.profiles.active=qa
PROD: mvn clean install -Dspring.profiles.active=prod
