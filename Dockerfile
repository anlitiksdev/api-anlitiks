FROM debian:latest

MAINTAINER RAKESH <r.jallapuram@reavidence.com>

WORKDIR /home

RUN  apt-get update -y
RUN apt-get install default-jre -y
RUN apt-get install default-jdk -y
RUN apt-get install git -y
RUN apt-get install maven -y

RUN git clone https://pavanummethala:qYzPqLRWwt5VzMhuGUSV@bitbucket.org/anlitiksdev/api-anlitiks.git

ARG APP_ENV_value

WORKDIR /home/api-anlitiks/


RUN mvn clean install -DskipTests -Dspring.profiles.active=$APP_ENV_value

EXPOSE 8076

WORKDIR /home

CMD java -jar -Dspring.profiles.active=$APP_ENV_value /home/api-anlitiks/target/api-anlitiks.jar; sleep infinity
