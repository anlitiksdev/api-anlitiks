package com.reavidence.anlitiks.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.reavidence.anlitiks.model.BusinessReq;

@Repository
public interface BusinessReqRepository extends CrudRepository<BusinessReq,Long> {
	
	@Query("SELECT t FROM BusinessReq t WHERE t.project.projectid = :projectId" )
    BusinessReq findByProjectId(@Param("projectId") Long projectId);

}
