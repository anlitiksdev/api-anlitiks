package com.reavidence.anlitiks.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.reavidence.anlitiks.model.LiteratureSearchKey;


@Repository
public interface LiteratureSearchKeyRepository extends CrudRepository<LiteratureSearchKey,Long> {
    @Query("select t.keyValue from LiteratureSearchKey t  where t.keyValue like %:keyValue% AND t.searchEngine = :searchEngine AND t.companyId = :companyId")
    public List<String> findByKeyValue(@Param("keyValue") String keyValue, @Param("searchEngine") String searchEngine, @Param("companyId") Integer companyId);

    @Query("select t from LiteratureSearchKey t where t.keyValue=:keyValue AND t.searchEngine = :searchEngine AND t.companyId = :companyId")
    public LiteratureSearchKey getByKeyValue(@Param("keyValue") String keyValue, @Param("searchEngine") String searchEngine, @Param("companyId") Integer companyId);
}
