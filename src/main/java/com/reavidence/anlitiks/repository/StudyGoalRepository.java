package com.reavidence.anlitiks.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.reavidence.anlitiks.model.StudyGoal;

@Repository
public interface StudyGoalRepository extends CrudRepository<StudyGoal, Long> {
    @Query("SELECT t FROM StudyGoal t WHERE t.project.projectid =:projectid" )
    StudyGoal findByProjectid(@Param("projectid") Long projectid);
}
