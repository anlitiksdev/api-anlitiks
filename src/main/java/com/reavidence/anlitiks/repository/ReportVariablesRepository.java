package com.reavidence.anlitiks.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.reavidence.anlitiks.model.ReportVariables;


@Repository
public interface ReportVariablesRepository extends CrudRepository<ReportVariables,Long> {
	
	@Query("select t from ReportVariables t join t.project p where p.projectid=:projectid")
    public List<ReportVariables> getByProjectId(@Param("projectid") Long projectid);
	
    @Modifying
    @Query("delete from  ReportVariables p  where p.id=:id")
    public void deleteReportVariablesByIdAndProjectId(@Param("id") Long id);
    
    @Query("select t from ReportVariables t where t.id=:id")
    public ReportVariables getById(@Param("id") Long id);
    
    @Query("select t from ReportVariables t join t.project p where p.projectid=:projectid and t.studyVariableId=:id")
    public ReportVariables getByProjectIdandAndStudyVariableId(@Param("projectid") Long projectid ,@Param("id") Long id);
    

}
