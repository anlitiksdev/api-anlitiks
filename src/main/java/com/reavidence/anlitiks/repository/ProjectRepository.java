package com.reavidence.anlitiks.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.reavidence.anlitiks.model.Project;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {
	
	@Query("SELECT t FROM Project t WHERE t.projectid =?1")
	public Project findByProjectid(@Param("projectid") Long projectid);

	@Query("SELECT t FROM Project t WHERE t.createdBy =?1")
	public List<Project> findByUserId(@Param("userid") Long userid);

	@Query("SELECT t FROM Project t WHERE t.createdBy = ?1 order by t.creationdate  desc")
	public List<Project> findByUserIdAndTimestamp(@Param("userid") Long userid);

	@Query("SELECT p FROM Project p WHERE p.companyId = :companyId")
	public List<Project> findByCompany(@Param("companyId") Integer companyId);

	@Query("SELECT p FROM Project p WHERE p.companyId = :companyId AND p.therapeuticAreaId = :areaId") 
	public List<Project> findByArea(@Param("companyId") Integer companyId, @Param("areaId") Integer areaId);

}
