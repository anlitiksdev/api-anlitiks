package com.reavidence.anlitiks.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.reavidence.anlitiks.model.InvitedProjects;

@Repository
public interface InvitedProjectsRepository extends CrudRepository<InvitedProjects, Integer> {

	@Query("SELECT ip FROM InvitedProjects ip WHERE ip.userId = :userId")
	List<InvitedProjects> findByuserId(@Param("userId") Long userId);
	
	@Query("SELECT ip FROM InvitedProjects ip WHERE ip.id= :id")
	public InvitedProjects findWithId(@Param("id") Integer id);
	
	

}
