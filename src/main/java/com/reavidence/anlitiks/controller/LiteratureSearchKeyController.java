package com.reavidence.anlitiks.controller;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.reavidence.anlitiks.common.CommonUtils;
import com.reavidence.anlitiks.model.LiteratureSearchKey;
import com.reavidence.anlitiks.repository.LiteratureSearchKeyRepository;
import com.reavidence.anlitiks.security.JwtTokenUtil;


@RestController
@RequestMapping(value = "litsearch")
public class LiteratureSearchKeyController {
	
	@Autowired
	private LiteratureSearchKeyRepository literatureSearchKeyRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;
	
	private static final Logger logger = LoggerFactory.getLogger(LiteratureSearchKeyController.class);
	
	@GetMapping("searchkey")
	public ResponseEntity<Object> searchKeyValue(HttpServletRequest request, @RequestParam String keyValue,
			@RequestParam String searchEngine) {
		logger.info("searching for keyvalues process started");
		try {
			logger.info("request deligated to dao layer");

			String authToken = request.getHeader(this.tokenHeader);

			logger.info("user auth token " + authToken);

			Integer companyId = jwtTokenUtil.getCompanyIdFromToken(authToken);
			
			return new ResponseEntity<>(literatureSearchKeyRepository.findByKeyValue(keyValue, searchEngine, companyId),HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1241", "litsearch/searchkey" ,exception),
					HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/savekeyvalue")
	public ResponseEntity<Object> saveKeyValue(HttpServletRequest request,
			@RequestBody LiteratureSearchKey literatureSearchKey) {
		logger.info("saving or update keyvalues process started");
		try {
			logger.info("request deligated to dao layer");

			String authToken = request.getHeader(this.tokenHeader);

			logger.info("user auth token " + authToken);

			Integer companyId = jwtTokenUtil.getCompanyIdFromToken(authToken);

			LiteratureSearchKey keyValue_found = literatureSearchKeyRepository
					.getByKeyValue(literatureSearchKey.getKeyValue(), literatureSearchKey.getSearchEngine(), companyId);
			if (keyValue_found != null) {
				return new ResponseEntity<>(
						 keyValue_found,
						HttpStatus.OK);
			} else {
				logger.info("request deligated to dao layer");
				literatureSearchKey.setCompanyId(companyId);
				return new ResponseEntity<>(
						literatureSearchKeyRepository.save(literatureSearchKey), HttpStatus.OK);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1242", "litsearch/savekeyvalue" ,exception),
					HttpStatus.BAD_REQUEST);
		}
	}

}
