package com.reavidence.anlitiks.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.reavidence.anlitiks.common.CommonUtils;
import com.reavidence.anlitiks.model.Project;
import com.reavidence.anlitiks.model.StudyGoal;
import com.reavidence.anlitiks.repository.StudyGoalRepository;


@RestController
@RequestMapping(value = "study-goal")
public class StudyGoalController {
	
	private static final Logger logger = LoggerFactory.getLogger(StudyGoalController.class);

	@Autowired
	private StudyGoalRepository studyGoalRepository;
	
	@GetMapping("{projectId}")
	public ResponseEntity<?> studyGoals(@PathVariable Long projectId) {
		logger.info("get all study goal process started");
		try {
			logger.info("request deligated to dao layer");
			
			StudyGoal studyGoal = studyGoalRepository.findByProjectid(projectId);
			
			return new ResponseEntity<>(studyGoal,HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1231", "study-goal/" +projectId,exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{projectId}/save")
	public ResponseEntity<?> save(@RequestBody StudyGoal studyGoal, @PathVariable Long projectId) {
		logger.info("save company process started");
		try {
			logger.info("request deligated to dao layer");

			studyGoal.setProject(getProject(projectId));
			
			return new ResponseEntity<>(studyGoalRepository.save(studyGoal), HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1232", "study-goal/" +projectId+"/save",exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/delete/{studyGoalId}")
	public ResponseEntity<?> delete(@PathVariable Long studyGoalId) {
		logger.info("delete study goal process started");
		try {

			logger.info("request deligated to dao layer");

			studyGoalRepository.deleteById(studyGoalId);

			logger.info("Study goal deleted successfully");

			return new ResponseEntity<>(HttpStatus.OK);	
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1233", "study-goal/delete/" +studyGoalId,exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	private Project getProject(Long projectId) {
		Project project = new Project();
		project.setProjectid(projectId);
		return project;
	}

}
