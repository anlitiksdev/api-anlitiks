package com.reavidence.anlitiks.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reavidence.anlitiks.common.CommonUtils;


@RestController
@RequestMapping(value="publish")
public class PublishController {
	
	private static final Logger logger = LoggerFactory.getLogger(PublishController.class);

	@Autowired
	private ProjectController projectController;
	
	@PostMapping("/{projectId}")
	public ResponseEntity<?> publishProject(@PathVariable Long projectId) {
		logger.info("finalizeProject process started");
		try {
			return new ResponseEntity<>(
    				 projectController.finalizeProject(projectId, "completed"), 
    				HttpStatus.OK);
		}catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1271", "spublish/" +projectId,exception),
					HttpStatus.BAD_REQUEST);
		}
		
	}

}
