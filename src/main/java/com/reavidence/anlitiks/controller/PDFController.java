package com.reavidence.anlitiks.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.reavidence.anlitiks.repository.ProjectRepository;

@RestController
@RequestMapping(value = "pdf")
@SuppressWarnings({ "unused", "unchecked" })
public class PDFController {
	
	private static final Logger logger = LoggerFactory.getLogger(PDFController.class);

	@Autowired
	private ProjectRepository projectRepository;
	
	@Value("${companyApiUrl}")
	private String CompanyURL;
	
	@PostMapping("/export-literature-articles")
	public void exportLiteratureArticles(HttpServletResponse response, @RequestBody Map<String, Object> payload)
			throws IOException, DocumentException {
		logger.info("Literature search articles pdf process started");

		try {
			response.setContentType("application/pdf");
			response.setHeader("content-disposition", "attachment; filename=literature-search.pdf");

			Rectangle layout = new Rectangle(PageSize.A2);
			layout.setBackgroundColor(new BaseColor(255, 255, 255));
			Document document = new Document(layout, 10, 10, 10, 10);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, baos);

			document.open();

			List<Object> data = (List<Object>) payload.get("data");
			logger.info("pdf operation from payload process started");
			for (int i = 0; i < data.size(); i++) {
				Map<String, Object> articles = (Map<String, Object>) data.get(i);
				document.add(this.createParagraph(i + 1 + " . " + articles.get("articleTitle").toString()));

				document.add(this.createParagraph(articles.get("journalList").toString()));

				document.add(this.createParagraph(articles.get("pmidList").toString()));
				document.add(this.createParagraph("\n"));

				document.add(this.createParagraph("Author Information:"));
				document.add(this.createParagraph(articles.get("authorList").toString()));
				document.add(this.createParagraph("\n"));

				document.add(this.createParagraph("Abstract:"));

				List<Object> abstractList = (List<Object>) articles.get("abstractList");
				for (int j = 0; j < abstractList.size(); j++) {
					Map<String, String> abstracts = (Map<String, String>) abstractList.get(j);
					String a1 = abstracts.get("_NlmCategory");
					if (a1 != null) {
						document.add(this.createParagraph(a1));
					}

					document.add(this.createParagraph(abstracts.get("__text")));
					document.add(this.createParagraph("\n"));
				}
			}
			document.close();
			logger.info("pdf operation from payload process completed");
			baos.writeTo(response.getOutputStream());
			logger.info("Literature search keyvalues pdf process completed");

		} catch (DocumentException e) {
			logger.error(e.getMessage(), e);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

		}
	}

	@PostMapping("/export-requirements")
	public void exportRequirements(HttpServletResponse response, @RequestBody Map<String, Object> payload)
			throws IOException, DocumentException {
		logger.info("Project Requirements pdf process started");
		try {
			response.setContentType("application/pdf");
			response.setHeader("content-disposition", "attachment; filename=requirements.pdf");
			Rectangle layout = new Rectangle(PageSize.A2);
			layout.setBackgroundColor(new BaseColor(255, 255, 255));
			Document document = new Document(layout, 10, 10, 10, 10);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, baos);

			document.open();

			Integer projectId = (Integer) payload.get("projectId");
			String type = (String) payload.get("type");
			String title = (String) payload.get("title");
			RestTemplate restTemplate = new RestTemplate();

			logger.info("Project id is" + projectId);

			com.reavidence.anlitiks.model.Project project = projectRepository.findByProjectid(new Long(projectId));

			Paragraph p = new Paragraph(project.getProjectName(), getBoldFont(18));
			p.setAlignment(Paragraph.ALIGN_CENTER);
			p.setSpacingBefore(50);
			document.add(p);

			document.add(createTitle(title + " : "));
			if (type.equalsIgnoreCase("projectDetails")) {
				document.add(createParagraph("\n"));

				document.add(createParagraph("Project Id : " + project.getProjectid().toString()));
				document.add(createParagraph("Project Name : " + project.getProjectName()));
				String sponsorObjectApiUrl = CompanyURL + "/sponsors/"+project.getSponsorId();
				JSONObject Sponsor = restTemplate.getForObject(sponsorObjectApiUrl, JSONObject.class);
				String SpFirstName = (String) Sponsor.get("firstName");
				String SpLastName = (String) Sponsor.get("lastName");
				String TherapeuticAreaObjectApiUrl = CompanyURL + "/areas/"+project.getTherapeuticAreaId();
				JSONObject TherapeuticArea = restTemplate.getForObject(TherapeuticAreaObjectApiUrl, JSONObject.class);
				String Area = (String) TherapeuticArea.get("areaName");
				document.add(createParagraph("Sponsor : " + (Sponsor != null
						? SpFirstName + " " + SpLastName: "")));
				document.add(createParagraph("Theraupeutic : "
						+ (TherapeuticArea != null ? Area : "")));
			} else {
				document.add(createParagraph("\n"));
				String data = (String) payload.get("data");

				StringBuilder htmlString = new StringBuilder();
				htmlString.append(new String("<html><body><style>" + "body{" + " font-family: 'Times New Roman';"
						+ "font-size: 16px;" + "margin: 30px;" + "}" + "h1,h2,h3,h4,h5,h6{" + "text-align: center;"
						+ "text-transform: uppercase;" + "font-weight: normal;" + "}" +
						"p{" + "text-indent: 50px;" + "margin-bottom: 30px;" + "line-height: 24px;"
						+ "margin-bottom: 30px;" + "}" +
						"ul{" + "padding-left: 30px;" + "}" +
						"ul li{" + "line-height: 24px;" + "}</style>"));
				htmlString.append(data);
				htmlString.append(new String("</body></html>"));

				InputStream is = new ByteArrayInputStream(htmlString.toString().getBytes());
				XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);

			}

			document.close();
			logger.info("pdf operation from payload process completed");
			baos.writeTo(response.getOutputStream());
			logger.info("Project Requirements pdf process completed");
		} catch (DocumentException e) {
			logger.error(e.getMessage(), e);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

		}
	}

	private Paragraph createParagraph(String value) {
		Font f = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
		return new Paragraph(value, f);
	}

	public static Font getBoldFont(Integer size) {
		return new Font(new Font(Font.FontFamily.TIMES_ROMAN, size, Font.BOLD, BaseColor.BLACK));
	}

	private static Element createTitle(String title) {
		PdfPTable innerTable = new PdfPTable(1);
		innerTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		innerTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		PdfPCell topCell = new PdfPCell();
		topCell.setBorder(0);
		topCell.setPaddingBottom(20f);
		topCell.addElement(new Paragraph(title, getBoldFont(18)));
		innerTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		innerTable.addCell(topCell);

		innerTable.completeRow();

		return innerTable;
	}

}
