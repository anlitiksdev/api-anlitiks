package com.reavidence.anlitiks.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.reavidence.anlitiks.common.CommonUtils;
import com.reavidence.anlitiks.model.InvitedProjects;
import com.reavidence.anlitiks.model.InvitedProjectsDto;
import com.reavidence.anlitiks.model.Project;
import com.reavidence.anlitiks.repository.InvitedProjectsRepository;
import com.reavidence.anlitiks.repository.ProjectRepository;
import com.reavidence.anlitiks.security.JwtTokenUtil;
import com.reavidence.anlitiks.utils.DateUtil;
import com.reavidence.anlitiks.utils.EmailHelper;
import com.reavidence.anlitiks.utils.UserRegisterDto;

@RestController
@RequestMapping(value = "invited-projects")
public class InvitedProjectsController {

	private static final Logger logger = LoggerFactory.getLogger(InvitedProjectsController.class);

	@Autowired
	private InvitedProjectsRepository invitedProjectsRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private ProjectRepository projectRepository;

	@Value("${companyApiUrl}")
	private String companyApiUrl;

	@GetMapping("")
	public ResponseEntity<?> invitedProjectss(HttpServletRequest request) {
		logger.info("invited projects by user id process started");
		try {

			String authToken = request.getHeader(this.tokenHeader);

			logger.info("user auth token " + authToken);

			Long userId = jwtTokenUtil.getUserIdFromToken(authToken);

			logger.info("user id " + userId);

			logger.info("request deligated to dao layer");

			List<InvitedProjects> list = invitedProjectsRepository.findByuserId(userId);

			logger.info("request processed successfully");

			return new ResponseEntity<>(list, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1251", "invited-projects", exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/save")
	public ResponseEntity<?> save(HttpServletRequest request,
			@RequestBody List<InvitedProjectsDto> invitedProjectsList) {
		logger.info("save request process started");

		try {

			if (invitedProjectsList.size() > 0) {

				String authToken = request.getHeader(this.tokenHeader);

				logger.info("user auth token " + authToken);

				Long userId = jwtTokenUtil.getUserIdFromToken(authToken);

				logger.info("user id " + userId);

				RestTemplate restTemplate = new RestTemplate();

				HttpHeaders headers = new HttpHeaders();
				headers.set(tokenHeader, authToken);

				HttpEntity<String> entity = new HttpEntity<>(headers);

				String userIdUrl = companyApiUrl + "/user/userid/" + userId;

				ResponseEntity<UserRegisterDto> loogedInUserResponseEntity = restTemplate.exchange(userIdUrl,
						HttpMethod.GET, entity, UserRegisterDto.class);

				for (InvitedProjectsDto invitedProjectsDto : invitedProjectsList) {
					String email = invitedProjectsDto.getUserEmail();
					String userEmailUrl = companyApiUrl + "/user/email/" + email;
					ResponseEntity<UserRegisterDto> inviteUserResponseEntity = restTemplate.exchange(userEmailUrl,
							HttpMethod.GET, entity, UserRegisterDto.class);
					UserRegisterDto inviteUserRegisterDto = inviteUserResponseEntity.getBody();

					if (inviteUserRegisterDto != null) {

						InvitedProjects invitedProjects = generateInvitedProject(invitedProjectsDto,
								new InvitedProjects());
						invitedProjects.setUserId(inviteUserRegisterDto.getId());
						invitedProjectsRepository.save(invitedProjects);

						new Thread() {
							UserRegisterDto loogedInUserRegisterDto = loogedInUserResponseEntity.getBody();

							public void run() {
								try {
									Project project = projectRepository
											.findByProjectid(invitedProjectsDto.getProjectId());
									emailHelper
											.sendInviteProjectEmail(inviteUserRegisterDto,
													loogedInUserRegisterDto.getFirstname() + " "
															+ loogedInUserRegisterDto.getLastname(),
													project.getProjectName());
								} catch (Exception e) {
									logger.error(e.getMessage(), e);
								}

							}
						}.start();

					}

				}

				logger.info("invited successfully");
				return new ResponseEntity<>("Invited successfully", HttpStatus.OK);

			} else {
				logger.error("No projects to save");
				String exception = "No projects to save";
				return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1252",
						"invited-projects/save", exception), HttpStatus.BAD_REQUEST);
			}
		} catch (DataAccessException e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1253", "invited-projects/save", exception),
					HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1253", "invited-projects/save", exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	private InvitedProjects generateInvitedProject(InvitedProjectsDto invitedProjectsDto,
			InvitedProjects invitedProjects) throws Exception {
		if (invitedProjectsDto.getId() != null && invitedProjectsDto.getId() instanceof Integer) {
			invitedProjects = invitedProjectsRepository.findWithId(invitedProjectsDto.getId());
			invitedProjects.setId(invitedProjectsDto.getId());
		}

		if (invitedProjectsDto.getProjectId() != null && invitedProjectsDto.getProjectId() instanceof Long) {
			invitedProjects.setProject(getProject(invitedProjectsDto.getProjectId()));
		}

		if (invitedProjectsDto.getExpiryDate() != null && !invitedProjectsDto.getExpiryDate().isEmpty()) {
			invitedProjects.setExpiryDate(
					DateUtil.string2Timestamp(invitedProjectsDto.getExpiryDate(), new SimpleDateFormat("yyyy-MM-dd")));
		} else {
			invitedProjects.setExpiryDate(new Timestamp(new Date().getTime()));
		}
		return invitedProjects;
	}

	private Project getProject(Long id) {
		Project project = new Project();
		project.setProjectid(id);
		return project;
	}

}
