package com.reavidence.anlitiks.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reavidence.anlitiks.common.CommonUtils;
import com.reavidence.anlitiks.model.BusinessReq;
import com.reavidence.anlitiks.model.Project;
import com.reavidence.anlitiks.repository.BusinessReqRepository;

@RestController
@RequestMapping(value = "business-requirements")
public class BusinessRequirementsController {
	
	private static final Logger logger = LoggerFactory.getLogger(BusinessRequirementsController.class);

	@Autowired
	private BusinessReqRepository buissinessReqRepository;
	
	@GetMapping("{projectId}")
	public ResponseEntity<?> requirements(@PathVariable Long projectId) {
		logger.info("get all requirements process started");
		try {
			logger.info("request deligated to dao layer");
			BusinessReq buissinessReq = buissinessReqRepository.findByProjectId(projectId);		
			return new ResponseEntity<>(buissinessReq,HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1221", "business-requirements/" +projectId,exception),
					HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/{projectId}/save")
	public ResponseEntity<?> save(@RequestBody BusinessReq buissinessReq, @PathVariable Long projectId) {
		logger.info("save Business requirements process started");
		try {
			logger.info("request deligated to dao layer");

			buissinessReq.setProject(getProject(projectId));
			buissinessReqRepository.save(buissinessReq);

			return new ResponseEntity<>(buissinessReqRepository.save(buissinessReq), HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1222", "business-requirements/" +projectId,exception),
					HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@DeleteMapping("/delete/{reqId}")
	public ResponseEntity<?> delete(@PathVariable Long reqId) {
		logger.info("delete business requirement process started");
		try {

			logger.info("request deligated to dao layer");

			buissinessReqRepository.deleteById(reqId);

			logger.info("Business requirements deleted successfully");
			
			return new ResponseEntity<>("Business requirements deleted successfully",HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1223", "business-requirements/delete" +reqId,exception),
					HttpStatus.BAD_REQUEST);
		}
	}
	

	private Project getProject(Long projectId) {
		Project project = new Project();
		project.setProjectid(projectId);
		return project;
	}

}
