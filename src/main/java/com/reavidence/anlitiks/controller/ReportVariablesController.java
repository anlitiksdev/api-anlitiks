package com.reavidence.anlitiks.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reavidence.anlitiks.common.CommonUtils;
import com.reavidence.anlitiks.model.ReportVariables;
import com.reavidence.anlitiks.repository.ProjectRepository;
import com.reavidence.anlitiks.repository.ReportVariablesRepository;

@RestController
@RequestMapping(value = "report_variable")
public class ReportVariablesController {
	
	private static final Logger logger = LoggerFactory.getLogger(ReportVariablesController.class);
	
    @Autowired
    private ReportVariablesRepository reportVariablesRepository;
    
    @Autowired
    private ProjectRepository projectRepository;
    
    /*[
    {
       "project" : {
        "projectid" : 1
       },
       "studyVariable" : {
         "id":1
       }
    },
       {
       "project" : {
        "projectid" : 1
      },
       "studyVariable" : {
         "id":1
       }
       }
]*/
    @PostMapping("/create")
    public ResponseEntity<?> saveKeyValue(@RequestBody List<ReportVariables> studyVariableJson) {
    	logger.info("save ReportVariables process started");
    	try {
    		logger.info("request deligated to dao layer");
        for (ReportVariables rv : studyVariableJson) {
            rv.setProject(projectRepository.findByProjectid(rv.getProject().getProjectid()));
            rv.setStudyVariableId(rv.getStudyVariableId());
            ReportVariables var = reportVariablesRepository.getByProjectIdandAndStudyVariableId(rv.getProject().getProjectid(),rv.getStudyVariableId());
            System.out.println(var);
            if(var!=null){   
                Map<String,String> m = new HashMap<>();
                m.put("ReportVariable","already exist");
                return new ResponseEntity<>(m,
        				HttpStatus.OK);
            }else{
                reportVariablesRepository.save(rv);
            }
        }
    	logger.info("reportVariable saved successfully");
        Long id = studyVariableJson.get(0).getProject().getProjectid();
        return new ResponseEntity<>(
				 reportVariablesRepository.getByProjectId(id),
				HttpStatus.OK);
    	}catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1262", "report_variable/create",exception),
					HttpStatus.BAD_REQUEST);
		}  
    }
    @GetMapping("by/{projectid}")
    public ResponseEntity<?> getByProjectId( @PathVariable Long projectid) {
    	logger.info("Getting ReportVariables by projectid  process started");
    try {
    	logger.info("request deligated to dao layer");
    	 return new ResponseEntity<>(
 			 reportVariablesRepository.getByProjectId(projectid),
 				HttpStatus.OK);	
        }catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1262", "report_variable/by/"+projectid,exception),
					HttpStatus.BAD_REQUEST);
		}     
    }
    @GetMapping("by/{projectid}/{id}")
    public ResponseEntity<?> getByProjectIdAndStudyVariableId(@PathVariable Long projectid ,@PathVariable Long id ){
    	logger.info("Getting ReportVariables by projectid and StudyVariableid  process started");
    	try {
    		logger.info("request deligated to dao layer");
    		return new ResponseEntity<>(
    	 				 reportVariablesRepository.getByProjectIdandAndStudyVariableId(projectid,id),
    	 				HttpStatus.OK);	
    	}catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1263", "report_variable/by/"+projectid+"/"+id,exception),
					HttpStatus.BAD_REQUEST);
		}   
    	}        
 
     @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteReportVariables(@PathVariable Long id){
    	logger.info("Deleting the ReportVariables process started");
    	try {
    		logger.info("request deligated to dao layer");
    		reportVariablesRepository.deleteReportVariablesByIdAndProjectId(id);
    		Map<String,String> m = new HashMap<>();
            m.put("ReportVariable","Successfully Deleted");
            logger.info("Deleting the ReportVariables process completed");
            return new ResponseEntity<>(m,	HttpStatus.OK);     		
    	}catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1264", "report_variable/delete/" +id,exception),
					HttpStatus.BAD_REQUEST);
		}           
    }

}
