package com.reavidence.anlitiks.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.reavidence.anlitiks.common.CommonUtils;
import com.reavidence.anlitiks.model.Project;
import com.reavidence.anlitiks.repository.ProjectRepository;
import com.reavidence.anlitiks.security.JwtTokenUtil;
import com.reavidence.anlitiks.utils.TherapeuticArea;
import com.reavidence.anlitiks.utils.UserRegisterDto;

@RestController
@RequestMapping(value = "projects")
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ProjectController {

	private static final Logger logger = LoggerFactory.getLogger(ProjectController.class);

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;

	@Value("${companyApiUrl}")
	private String companyApiUrl;

	@GetMapping("{roleId}/{companyId}")
	public ResponseEntity<?> projects(HttpServletRequest request, @PathVariable Integer roleId,
			@PathVariable Integer companyId) {
		logger.info("get all companies process started");
		try {
			String authToken = request.getHeader(this.tokenHeader);

			logger.info("user auth token " + authToken);

			logger.info("request deligated to dao layer");

			List<Project> list = new ArrayList<>();

			if (roleId == 2) {
				list = projectRepository.findByCompany(companyId);
			} else if (roleId > 2) {

				Integer areaId = jwtTokenUtil.getAreaIdFromToken(authToken);

				logger.info("area id " + areaId);

				list = projectRepository.findByArea(companyId, areaId);

			} else {
				list = projectRepository.findByCompany(companyId);
			}

			RestTemplate userRestTemplate = new RestTemplate(getClientHttpRequestFactory());
			RestTemplate areaRestTemplate = new RestTemplate(getClientHttpRequestFactory());
			HttpHeaders headers = new HttpHeaders();
			headers.set(tokenHeader, authToken);

			HttpEntity<String> entity = new HttpEntity<>(headers);

			for (Project project : list) {
				String userURI = companyApiUrl + "/user/userid/" + project.getCreatedBy();
				String areaURI = companyApiUrl + "/areas/" + project.getTherapeuticAreaId();

				logger.info(" userURI ==> " + userURI);
				logger.info(" areaURI ==> " + areaURI);

				ResponseEntity<UserRegisterDto> userResponseEntity = userRestTemplate.exchange(userURI, HttpMethod.GET,
						entity, UserRegisterDto.class);
				project.setUser(userResponseEntity.getBody());

				ResponseEntity<TherapeuticArea> areaResponseEntity = areaRestTemplate.exchange(areaURI, HttpMethod.GET,
						entity, TherapeuticArea.class);
				project.setTherapeuticArea(areaResponseEntity.getBody());

			}
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1211",
					"projects/" + roleId + "/" + companyId, exception), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("find/{projectId}")
	public ResponseEntity<?> find(@PathVariable Long projectId) {
		logger.info("get all companies process started");
		try {

			logger.info("request deligated to dao layer");

			Project project = projectRepository.findById(projectId).get();

			return new ResponseEntity<>(project, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(
					CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1212", "projects/" + projectId, exception),
					HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping("{roleId}/save")
	public ResponseEntity<?> save(HttpServletRequest request, @PathVariable Integer roleId,
			@RequestBody Project project) {
		logger.info("save company process started");
		try {

			String authToken = request.getHeader(this.tokenHeader);

			logger.info("user auth token " + authToken);

			Integer companyId = jwtTokenUtil.getCompanyIdFromToken(authToken);

			Long userId = jwtTokenUtil.getUserIdFromToken(authToken);

			logger.info("companyId id " + companyId);

			logger.info("request deligated to dao layer");

			project.setCompanyId(companyId);
			project.setCreatedBy(userId);

			if (project.getSponsorId() != null && project.getSponsorId() instanceof Integer) {
				project.setSponsorId(project.getSponsorId());
			}
			if (roleId == 2) {
				if (project.getTherapeuticAreaId() != null && project.getTherapeuticAreaId() instanceof Integer) {
					project.setTherapeuticAreaId(project.getTherapeuticAreaId());
				}
			} else if (roleId > 2) {
				Integer areaId = jwtTokenUtil.getAreaIdFromToken(authToken);
				logger.info("area id " + areaId);
				project.setTherapeuticAreaId(areaId);
			}
			projectRepository.save(project);
			logger.info("project registered successfully");

			return new ResponseEntity<>(project, HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1213",
					"projects/" + roleId + "/save", exception), HttpStatus.BAD_REQUEST);
		}

	}

	@PostMapping("/update/{projectId}/{type}/{percentage}")
	public ResponseEntity<?> updateProject(@PathVariable Long projectId, @PathVariable String type,
			@PathVariable int percentage) {
		logger.info("get all companies process started");
		try {
			Project project = projectRepository.findByProjectid(projectId);
			if (project != null) {
				project.setType(type);
				project.setPercentage(percentage);
				return new ResponseEntity(projectRepository.save(project), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1212",
						"projects/update/" + projectId + "/" + type, null), HttpStatus.BAD_REQUEST);

			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1212",
					"projects/complete/" + projectId + "/" + type, exception), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/complete/{projectId}/{status}")
	public ResponseEntity<?> finalizeProject(@PathVariable Long projectId, @PathVariable String status) {
		logger.info("get all companies process started");
		try {
			Project project = projectRepository.findByProjectid(projectId);
			if (project != null) {
				project.setProjectStatus(status);
				return new ResponseEntity(projectRepository.save(project), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1212",
						"projects/complete/" + projectId + "/" + status, null), HttpStatus.BAD_REQUEST);

			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			String exception = e.getMessage();
			return new ResponseEntity<>(CommonUtils.getApiErrorResponse(HttpStatus.BAD_REQUEST, "1212",
					"projects/complete/" + projectId + "/" + status, exception), HttpStatus.BAD_REQUEST);
		}
	}

	// Override timeouts in request factory
	private SimpleClientHttpRequestFactory getClientHttpRequestFactory() {
		SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
		// Connect timeout
		clientHttpRequestFactory.setConnectTimeout(10_000);

		// Read timeout
		clientHttpRequestFactory.setReadTimeout(6 * 10000);
		return clientHttpRequestFactory;
	}
}
