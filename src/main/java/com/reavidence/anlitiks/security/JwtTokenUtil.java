package com.reavidence.anlitiks.security;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Component
public class JwtTokenUtil implements Serializable {

	private static final long serialVersionUID = -3301605591108950415L;

	static final String CLAIM_KEY_USERNAME = "sub";
	static final String CLAIM_KEY_AUDIENCE = "audience";
	static final String CLAIM_KEY_CREATED = "created";
	static final String CLAIM_KEY_USER_ID = "userId";
	static final String CLAIM_KEY_COMPANY_ID = "companyId";
	static final String CLAIM_KEY_AREA_ID = "areaId";

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.expiration}")
	private Long expiration;

	public Long getUserIdFromToken(String token) {
		Integer userId;
		try {
			final Claims claims = getClaimsFromToken(token);
			userId = (Integer) claims.get(CLAIM_KEY_USER_ID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			userId = null;
		}
		return userId != null ? new Long(userId) : null;
	}

	public Integer getCompanyIdFromToken(String token) {
		Integer companyId;
		try {
			final Claims claims = getClaimsFromToken(token);
			companyId = (Integer) claims.get(CLAIM_KEY_COMPANY_ID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			companyId = null;
		}
		return companyId != null ? companyId : null;
	}

	public Integer getAreaIdFromToken(String token) {
		Integer areaId;
		try {
			final Claims claims = getClaimsFromToken(token);
			areaId = (Integer) claims.get(CLAIM_KEY_AREA_ID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			areaId = null;
		}
		return areaId != null ? areaId : null;
	}

	public String getUsernameFromToken(String token) {
		String username;
		try {
			final Claims claims = getClaimsFromToken(token);
			username = claims.getSubject();
		} catch (Exception e) {
			username = null;
		}
		return username;
	}

	public Date getCreatedDateFromToken(String token) {
		Date created;
		try {
			final Claims claims = getClaimsFromToken(token);
			created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
		} catch (Exception e) {
			created = null;
		}
		return created;
	}

	public Date getExpirationDateFromToken(String token) {
		Date expiration;
		try {
			final Claims claims = getClaimsFromToken(token);
			expiration = claims.getExpiration();
		} catch (Exception e) {
			expiration = null;
		}
		return expiration;
	}

	public String getAudienceFromToken(String token) {
		String audience;
		try {
			final Claims claims = getClaimsFromToken(token);
			audience = (String) claims.get(CLAIM_KEY_AUDIENCE);
		} catch (Exception e) {
			audience = null;
		}
		return audience;
	}

	private Claims getClaimsFromToken(String token) {
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			claims = null;
		}
		return claims;
	}

	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	public Boolean validateToken(String token, UserDetails userDetails) {
		JwtUser user = (JwtUser) userDetails;
		final String username = getUsernameFromToken(token);
		// final Date created = getCreatedDateFromToken(token);
		// final Date expiration = getExpirationDateFromToken(token);
		return (username.equals(user.getUsername()) && !isTokenExpired(token));
	}
}
