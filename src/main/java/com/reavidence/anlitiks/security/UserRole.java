package com.reavidence.anlitiks.security;

import java.io.Serializable;

public class UserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private Role role;

	private User user;

	public UserRole() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}