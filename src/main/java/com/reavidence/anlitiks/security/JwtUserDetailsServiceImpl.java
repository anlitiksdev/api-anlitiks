package com.reavidence.anlitiks.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Sandeep on 10/31/2017.
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = new User();
		user.setEmail(username);

		List<UserRole> userRoles = new ArrayList<UserRole>();

		user.setUserRoles(userRoles);

		return JwtUserFactory.create(user);
	}
}
