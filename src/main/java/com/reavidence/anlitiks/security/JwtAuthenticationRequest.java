package com.reavidence.anlitiks.security;

import java.io.Serializable;

/**
 * Created by Sandeep on 10/31/2017.
 */
public class  JwtAuthenticationRequest implements Serializable {

    private static final long serialVersionUID = -8445943548965154778L;

    private String email;
    private String password;

    public JwtAuthenticationRequest() {
        super();
    }

    public JwtAuthenticationRequest(String email, String password) {
        this.setEmail(email);
        this.setPassword(password);
    }

    public void setEmail(String email) {
		this.email = email;
	}
    
    public String getEmail() {
		return email;
	}

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

