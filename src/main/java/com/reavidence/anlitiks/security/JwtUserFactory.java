package com.reavidence.anlitiks.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 * Created by Sandeep on 10/31/2017.
 */
public final class JwtUserFactory {

	private JwtUserFactory() {
	}

	public static JwtUser create(User user) {
		return new JwtUser(user.getId(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(),
				mapToGrantedAuthorities(user.getUserRoles()), user.isEnabled(), user.getLastpasswordresetdate());
	}

	private static List<GrantedAuthority> mapToGrantedAuthorities(List<UserRole> authorities) {
		if(authorities != null && authorities.size() > 0){
			return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getRole().getRoleName()))
					.collect(Collectors.toList());
		}else{
			return new ArrayList<GrantedAuthority>();
		}
		
	}
}
