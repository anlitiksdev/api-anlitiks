package com.reavidence.anlitiks.common;

import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;



public class CommonUtils implements MessageSourceAware {
	

	public static MessageSource messageSource;
	static String ERROR_MESSAGE = null;

	public CommonUtils() {
	}

	public static <T> ApiErrorResponse getApiErrorResponse(HttpStatus httpStatus, String errorCode, String path) {
		ApiErrorResponse errorResponse = new ApiErrorResponse();
		errorResponse.setErrorCode(errorCode);
		errorResponse.setError(httpStatus.getReasonPhrase());
		errorResponse.setMessage(messageSource.getMessage(errorCode, null, Locale.ENGLISH));
		errorResponse.setTimestamp(new Date().toString());
		errorResponse.setPath(path);
		errorResponse.setStatus(httpStatus.value());
		return errorResponse;

	}

	public static <T> ApiErrorResponse getApiErrorResponse(HttpStatus httpStatus, String errorCode,
			String path,String exception) {
		ApiErrorResponse errorResponse = new ApiErrorResponse();
		errorResponse.setError(httpStatus.getReasonPhrase());
		errorResponse.setMessage(messageSource.getMessage(errorCode, null, Locale.ENGLISH));
		errorResponse.setException(exception);
		errorResponse.setErrorCode(errorCode);
		errorResponse.setTimestamp(new Date().toString());
		errorResponse.setPath(path);
		errorResponse.setStatus(httpStatus.value());
		return errorResponse;
	}

	public static String getErrorMessage(BindingResult bindingResult) {

		return ERROR_MESSAGE = bindingResult.getAllErrors().stream().map(objectError -> objectError.getDefaultMessage())
				.collect(Collectors.joining(","));
	}

	@SuppressWarnings("static-access")
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
