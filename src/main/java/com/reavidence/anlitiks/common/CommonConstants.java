package com.reavidence.anlitiks.common;

public interface CommonConstants {
	
	//Email related constants
	
		String EMAIL_ENCODING = "UTF-8";
		public static final String EMAIL_ACCOUNT_NAME = "anlitiks";
		public static final String EMAIL_ACCOUNT_ID = "dontreply@reavidence.com";

}
