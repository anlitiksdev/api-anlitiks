package com.reavidence.anlitiks.utils;

import java.util.List;


public class UserRegisterDto {

	private Long id;
	private String email;
	private String firstname;
	private String lastname;
	private String password;
	private String oldPassword;
	private String token;

	private Boolean enabled;
	private String phone;
	private String address;
	private String country;
	private String state;
	private String city;

	private String zip;

	private List<RolesDto> roles;
	

	private Integer companyId;
	private Integer therapeuticAreaId;
	private Integer licenses;

	private String inviteMessage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRoles(List<RolesDto> roles) {
		this.roles = roles;
	}

	public List<RolesDto> getRoles() {
		return roles;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Integer getTherapeuticAreaId() {
		return therapeuticAreaId;
	}

	public void setTherapeuticAreaId(Integer therapeuticAreaId) {
		this.therapeuticAreaId = therapeuticAreaId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}



	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setLicenses(Integer licenses) {
		this.licenses = licenses;
	}

	public Integer getLicenses() {
		return licenses;
	}

	public void setInviteMessage(String inviteMessage) {
		this.inviteMessage = inviteMessage;
	}

	public String getInviteMessage() {
		return inviteMessage;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}
}
