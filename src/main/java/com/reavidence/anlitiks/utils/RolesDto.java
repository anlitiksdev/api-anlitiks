package com.reavidence.anlitiks.utils;

public class RolesDto {

	private Integer roleId;
	private Integer id;
	private String RoleName;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	public String getRoleName() {
		return RoleName;
	}

}
