package com.reavidence.anlitiks.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

public abstract class TinyURLHelper {
	public static String getTinyUrl( String fullUrl ) throws HttpException, IOException {

		HttpClient httpclient = new HttpClient();
		HttpMethod method = new GetMethod( "http://tinyurl.com/api-create.php" ); 
		method.setQueryString( new NameValuePair[] { new NameValuePair( "url", fullUrl ) } );
		httpclient.executeMethod( method );

		InputStream is = method.getResponseBodyAsStream();
		StringBuffer sb = new StringBuffer();
		for( int i = is.read(); i != -1; i = is.read() ) {
			sb.append( (char) i );
		}

		String tinyUrl = sb.toString();
		method.releaseConnection();
		return tinyUrl;
	}
}
