package com.reavidence.anlitiks.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.httpclient.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.RestTemplate;

import com.reavidence.anlitiks.common.CommonConstants;
import com.reavidence.anlitiks.security.JwtTokenUtil;
import com.reavidence.anlitiks.security.User;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
public class EmailHelper {
	
	@Value("${web.url}")
	private String appUrl;

	@Autowired
	private Configuration freemarkerConfig;

	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Value("${companyApiUrl}")
	private String URL;
	
	public void sendUserRegisterationEmail(String authToken) throws HttpException, IOException {
		MimeMessage message = mailSender.createMimeMessage();
		Map<String, Object> model = new HashMap<String, Object>();
		Long userId = jwtTokenUtil.getUserIdFromToken(authToken);
		RestTemplate restTemplate = new RestTemplate();
		String userUrl = URL+"/user/"+userId;
		User user = restTemplate.getForObject(userUrl, User.class);
		model.put("user", user);
		model.put("url", TinyURLHelper.getTinyUrl(appUrl + "#/confirmation?token=" + authToken));

		try {
			// Using a subfolder such as /templates here
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

			Template t = freemarkerConfig.getTemplate("regestration.ftl");

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			message.setFrom(new InternetAddress(CommonConstants.EMAIL_ACCOUNT_ID, CommonConstants.EMAIL_ACCOUNT_NAME));
			helper.setTo(user.getEmail());
			helper.setSubject("New Anlitiks account");
			helper.setText(html, true);

			mailSender.send(message);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (TemplateException e) {
			throw new MailParseException(e);
		}

	}

	public void sendCompanyAdminUserRegisterationEmail(String authToken, User user) throws HttpException, IOException {
		MimeMessage message = mailSender.createMimeMessage();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("url", TinyURLHelper.getTinyUrl(appUrl + "#/confirmation?token=" + authToken));

		try {
			// Using a subfolder such as /templates here
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

			Template t = freemarkerConfig.getTemplate("admin-regestration.ftl");

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			message.setFrom(new InternetAddress(CommonConstants.EMAIL_ACCOUNT_ID, CommonConstants.EMAIL_ACCOUNT_NAME));
			helper.setTo(user.getEmail());
			helper.setSubject("Company account created!");
			helper.setText(html, true);

			mailSender.send(message);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (TemplateException e) {
			throw new MailParseException(e);
		}

	}
	
	public void sendInviteUsersEmail(String authToken, User user, String companyAdminName)
			throws HttpException, IOException {
		MimeMessage message = mailSender.createMimeMessage();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("companyAdminName", companyAdminName);
		model.put("url", TinyURLHelper.getTinyUrl(appUrl + "#/confirmation?token=" + authToken));

		try {
			// Using a subfolder such as /templates here
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

			Template t = freemarkerConfig.getTemplate("invite-users.ftl");

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			message.setFrom(new InternetAddress(CommonConstants.EMAIL_ACCOUNT_ID, CommonConstants.EMAIL_ACCOUNT_NAME));
			helper.setTo(user.getEmail());
			helper.setSubject("Invitation to new Anlitiks account");
			helper.setText(html, true);

			mailSender.send(message);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (TemplateException e) {
			throw new MailParseException(e);
		}

	}

	public void sendPasswordChangeSuccessNotification(User user) throws HttpException, IOException {
		MimeMessage message = mailSender.createMimeMessage();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("date", DateUtil.calendar2String(Calendar.getInstance(),
				new SimpleDateFormat("EEEEE MMMMM yyyy HH:mm:ss.SSSZ")));

		try {
			// Using a subfolder such as /templates here
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

			Template t = freemarkerConfig.getTemplate("password-success-notification.ftl");

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			message.setFrom(new InternetAddress(CommonConstants.EMAIL_ACCOUNT_ID, CommonConstants.EMAIL_ACCOUNT_NAME));
			helper.setTo(user.getEmail());
			helper.setSubject("Confirmation on Password change");
			helper.setText(html, true);

			mailSender.send(message);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (TemplateException e) {
			throw new MailParseException(e);
		}

	}

	public void sendInviteProjectEmail(UserRegisterDto user, String invitedName, String projectName) throws HttpException, IOException {
		MimeMessage message = mailSender.createMimeMessage();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("projectName", projectName);
		model.put("invitedName", invitedName);

		try {
			// Using a subfolder such as /templates here
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

			Template t = freemarkerConfig.getTemplate("project-invitation.ftl");

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			message.setFrom(new InternetAddress(CommonConstants.EMAIL_ACCOUNT_ID, CommonConstants.EMAIL_ACCOUNT_NAME));
			helper.setTo(user.getEmail());
			helper.setSubject("Invitation to Project");
			helper.setText(html, true);

			mailSender.send(message);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (TemplateException e) {
			throw new MailParseException(e);
		}

	}
	
	public void sendForgotpasswordEmail(String authToken, User user) throws HttpException, IOException {
		MimeMessage message = mailSender.createMimeMessage();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		model.put("url", TinyURLHelper.getTinyUrl(appUrl + "#/confirmation?token=" + authToken));

		try {
			// Using a subfolder such as /templates here
			freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

			Template t = freemarkerConfig.getTemplate("forgot-password.ftl");

			String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			message.setFrom(new InternetAddress(CommonConstants.EMAIL_ACCOUNT_ID, CommonConstants.EMAIL_ACCOUNT_NAME));
			helper.setTo(user.getEmail());
			helper.setSubject("Forgot password your password on Anlitiks?");
			helper.setText(html, true);

			mailSender.send(message);
		} catch (MessagingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e.getMessage());
			throw new MailParseException(e);
		} catch (TemplateException e) {
			throw new MailParseException(e);
		}

	}

}
