package com.reavidence.anlitiks.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DateUtil {
	
	static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	static SimpleDateFormat tzone = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");

	/*
	 * This Method is used to Converting String to Date. It takes two parameters
	 * one for string another one is format of that string and returns Date of
	 * the given string into the Corresponding given format.
	 */
	

	public static Date string2Date(String s, SimpleDateFormat simpleDateFormat) throws Exception {
		return simpleDateFormat.parse(s);
	}

	/*
	 * This Method is used to Converting Date to Calendar. It takes one
	 * parameters that is date and returns Calendar of the given date.
	 */

	public static Calendar date2Calendar(Date date) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	/*
	 * This Method is used to Converting String to Calendar. It takes two
	 * parameters one for string another one is format of that string and
	 * returns Calendar of the given string into the Corresponding given format.
	 */

	public static Calendar string2Calendar(String s, SimpleDateFormat simpleDateFormat) throws ParseException {
		Calendar cal = Calendar.getInstance();
		Date d1 = simpleDateFormat.parse(s);
		cal.setTime(d1);
		return cal;
	}

	/*
	 * This Method is used to Converting TimeStamp to Date. It takes one
	 * parameters that is TimeStamp and returns Date of the given TimeStamp.
	 */

	public static Date timeStamp2Date(Timestamp timestamp) {
		return new Date(timestamp.getTime());
	}

	/*
	 * This Method is used to Converting String to TimeStamp. It takes two
	 * parameters one for string another one is format of that string and
	 * returns TimeStamp of the given string into the Corresponding given
	 * format.
	 */

	public static Timestamp string2Timestamp(String string, SimpleDateFormat simpleDateFormat) throws Exception {
		Date date = (Date) simpleDateFormat.parse(string);
		return new Timestamp(date.getTime());
	}

	/*
	 * This Method is used to Converting Date to String. It takes two parameters
	 * one for Date another one is format of that given Date and returns String
	 * of the given Date into the Corresponding given format.
	 */
	public static String date2String(Date date, SimpleDateFormat simpleDateFormat) {

		return simpleDateFormat.format(date);
	}

	/*
	 * This Method is used to Converting Calendar to String. It takes two
	 * parameters one for Calendar another one is format of that given Calendar
	 * and returns String of the given Calendar into the Corresponding given
	 * format.
	 */

	public static String calendar2String(Calendar calendar, SimpleDateFormat simpleDateFormat) {

		return simpleDateFormat.format(calendar.getTime());
	}

	/*
	 * This Method is used to find out the days between two Calendar objects.
	 * The Calendar objects are passed as a parameters.
	 */

	public static long daysBetween2CalendarObjects(Calendar startCalendar, Calendar endCalendar) {

		Calendar date = (Calendar) startCalendar.clone();

		long daysBetween = 0;
		while (date.before(endCalendar)) {
			date.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return daysBetween;
	}

	/*
	 * This Method is used to find out the days between two Date objects. The
	 * Date objects are passed as a parameters.
	 */
	public static long daysBetween2DateObjects(Date startDate, Date endDate) throws Exception {
		long timeDiff = Math.abs(startDate.getTime() - endDate.getTime());
		return timeDiff;
	}

	/*
	 * This Method is used to find out the enter string date is equal to today
	 * date or not. It takes two parameters one for string date another one is
	 * format of that given string date and it returns true/false.
	 */
	public static boolean checkDateStringEqual2Today(String strinDate, SimpleDateFormat simpleDateFormat)
			throws Exception {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = null;
		String todaydate = calendar2String(cal1, simpleDateFormat);
		try {
			cal1 = string2Calendar(todaydate, simpleDateFormat);
			cal2 = string2Calendar(strinDate, simpleDateFormat);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (cal1.equals(cal2))
			return true;
		else
			return false;
	}

	public static Timestamp getCurrentTimestamp() {
		return new Timestamp(Calendar.getInstance().getTime().getTime());
	}

	public static Timestamp calender2Timestamp(Calendar calendar) {
		return new Timestamp(calendar.getTime().getTime());
	}

	public static String Timestamp2String(Timestamp timestampDate, SimpleDateFormat df) {
		Date date = timeStamp2Date(timestampDate);
		return date2String(date, df);
	}

	public static String long2String(String stringDate, SimpleDateFormat df) {
		Long longDate = Long.parseLong(stringDate);
		Timestamp timestampDate = new Timestamp(longDate);
		Date date = timeStamp2Date(timestampDate);
		return date2String(date, df);
	}

	public static List<String> getMonthsInBetweenTwoDates(String startDate, String endDate) {
		List<String> monthsData = new ArrayList<String>();
		DateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat formater1 = new SimpleDateFormat("MMM-yyyy");

		Calendar beginCalendar = Calendar.getInstance();
		Calendar finishCalendar = Calendar.getInstance();

		try {
			beginCalendar.setTime(formater.parse(startDate));
			finishCalendar.setTime(formater.parse(endDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		while (beginCalendar.before(finishCalendar) || beginCalendar.equals(finishCalendar)) {
			// add one month to date per loop
			String date = formater1.format(beginCalendar.getTime()).toUpperCase();
			monthsData.add(date);
			beginCalendar.add(Calendar.MONTH, 1);
		}
		return monthsData;
	}

	public static void main(String[] args) throws Exception {

		String date1 = "2012-01-01";
		String date2 = "2017-02-01";
		
		List<String> data = DateUtil.getMonthsInBetweenTwoDates(date1, date2);
		System.out.println(data.toString());

		/*
		 * try{
		 * 
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); Date date1
		 * = sdf.parse("2013-04-09"); Date date2 = sdf.parse("2013-05-09");
		 * 
		 * System.out.println(sdf.format(date1));
		 * System.out.println(sdf.format(date2));
		 * 
		 * if(date1.compareTo(date2)>0){
		 * System.out.println("Date1 is after Date2"); }else
		 * if(date1.compareTo(date2)<0){
		 * System.out.println("Date1 is before Date2"); }else
		 * if(date1.compareTo(date2)==0){
		 * System.out.println("Date1 is equal to Date2"); }else{
		 * System.out.println("How to get here?"); }
		 * 
		 * }catch(ParseException ex){ ex.printStackTrace(); }
		 */

		// String hashValue = MD5.getHashString("instaR04nds!");
		// System.out.println("current timestamp ====>"+getCurrentTimestamp());
	}
	private static List<SimpleDateFormat> dateFormats = new ArrayList<SimpleDateFormat>() {
		private static final long serialVersionUID = 1L; 
		{
            add(new SimpleDateFormat("M/dd/yyyy"));
            add(new SimpleDateFormat("dd.M.yyyy"));
            add(new SimpleDateFormat("M/dd/yyyy hh:mm:ss a"));
            add(new SimpleDateFormat("dd.M.yyyy hh:mm:ss a"));
            add(new SimpleDateFormat("dd.MMM.yyyy"));
            add(new SimpleDateFormat("dd-MMM-yyyy"));
        }
    };
    
    public static Date convertToDate(String input) {
        Date date = null;
        if(null == input) {
            return null;
        }
        for (SimpleDateFormat format : dateFormats) {
            try {
            	format.setLenient(false);
                date = format.parse(input);
            } catch (ParseException e) {
                //Shhh.. try other formats
            }
            if (date != null) {
                break;
            }
        }
 
        return date;
    }


}
