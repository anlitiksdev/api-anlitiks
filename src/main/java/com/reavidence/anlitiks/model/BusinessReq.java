package com.reavidence.anlitiks.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name = "buissiness_requirement")
public class BusinessReq {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@JsonIgnore
	@OneToOne
	private Project project;
	
	@Column(columnDefinition = "TEXT")
	private String buissiness_problem;

	@Column(columnDefinition = "TEXT")
	private String buissiness_goals;

	@Column(columnDefinition = "TEXT")
	private String notes;

	// TODO Change to logged in User object mapping.
	private Long createdby;

	private Date creationdate = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getBuissiness_problem() {
		return buissiness_problem;
	}

	public void setBuissiness_problem(String buissiness_problem) {
		this.buissiness_problem = buissiness_problem;
	}

	public String getBuissiness_goals() {
		return buissiness_goals;
	}

	public void setBuissiness_goals(String buissiness_goals) {
		this.buissiness_goals = buissiness_goals;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getCreatedby() {
		return createdby;
	}

	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
}
