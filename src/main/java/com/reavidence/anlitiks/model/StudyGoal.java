package com.reavidence.anlitiks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sreenivasulu Akkem on 02/03/2017.
 */
@Entity
@Table(name = "study_goal",uniqueConstraints = {@UniqueConstraint(columnNames = {"project_projectid"})})
public class StudyGoal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@JsonIgnore
	@OneToOne
	private Project project;
	@Column(columnDefinition="TEXT")
	private  String formulate_study;
	@Column(columnDefinition="TEXT")
	private  String hypothesis_study;
	@Column(columnDefinition="TEXT")
	private  String study_objectives;
	private  String notes;
	//TODO change it to user!
	private  Integer createdby;
	private Date creationdate = new Date();


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public String getFormulate_study() {
		return formulate_study;
	}
	public void setFormulate_study(String formulate_study) {
		this.formulate_study = formulate_study;
	}
	public String getHypothesis_study() {
		return hypothesis_study;
	}
	public void setHypothesis_study(String hypothesis_study) {
		this.hypothesis_study = hypothesis_study;
	}
	public String getStudy_objectives() {
		return study_objectives;
	}
	public void setStudy_objectives(String study_objectives) {
		this.study_objectives = study_objectives;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Integer getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}
	public Date getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	
}
