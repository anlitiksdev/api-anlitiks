package com.reavidence.anlitiks.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ReportVariables {
	
	@Id

    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;
    
    private Long studyVariableId;
    @ManyToOne
    private Project project;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
   

    public Long getStudyVariableId() {
		return studyVariableId;
	}

	public void setStudyVariableId(Long studyVariableId) {
		this.studyVariableId = studyVariableId;
	}

	public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }




}
