package com.reavidence.anlitiks.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LiteratureSearchKey {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Long id;
	    
	    private String keyValue;
	    
	    private String searchEngine;
	    
	    private Integer companyId;
	    
	    private Date createdTimeStamp = new Date();
	    
	    public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public String getKeyValue() {
	        return keyValue;
	    }

	    public void setKeyValue(String keyValue) {
	        this.keyValue = keyValue;
	    }

	    public String getSearchEngine() {
	        return searchEngine;
	    }

	    public void setSearchEngine(String searchEngine) {
	        this.searchEngine = searchEngine;
	    }

	    public Date getCreatedTimeStamp() {
	        return createdTimeStamp;
	    }

	    public void setCreatedTimeStamp(Date createdTimeStamp) {
	        this.createdTimeStamp = createdTimeStamp;
	    }

		public Integer getCompanyId() {
			return companyId;
		}

		public void setCompanyId(Integer companyId) {
			this.companyId = companyId;
		}
	    
	    

}
