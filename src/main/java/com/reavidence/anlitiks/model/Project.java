package com.reavidence.anlitiks.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.reavidence.anlitiks.utils.TherapeuticArea;
import com.reavidence.anlitiks.utils.UserRegisterDto;

/**
 * @author hp
 *
 */
@Entity
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long projectid;

	private Timestamp creationdate;

	private Timestamp lastupdateddate;

	@Column(name = "project_name")
	private String projectName;

	@Column(name = "project_status")
	private String projectStatus;

	private Integer companyId;

	@Column(name = "sponsor_id")
	private Integer sponsorId;

	@Column(name = "area_id")
	private Integer therapeuticAreaId;

	private Integer percentage;

	private String type;

	private Long createdBy;

	@Transient
	private UserRegisterDto user;

	@Transient
	private TherapeuticArea therapeuticArea;

	public Project() {
	}

	public Long getProjectid() {
		return this.projectid;
	}

	public void setProjectid(Long projectid) {
		this.projectid = projectid;
	}

	public Timestamp getCreationdate() {
		return this.creationdate;
	}

	public void setCreationdate(Timestamp creationdate) {
		this.creationdate = creationdate;
	}

	public Timestamp getLastupdateddate() {
		return this.lastupdateddate;
	}

	public void setLastupdateddate(Timestamp lastupdateddate) {
		this.lastupdateddate = lastupdateddate;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getSponsorId() {
		return sponsorId;
	}

	public void setSponsorId(Integer sponsorId) {
		this.sponsorId = sponsorId;
	}

	public Integer getTherapeuticAreaId() {
		return therapeuticAreaId;
	}

	public void setTherapeuticAreaId(Integer therapeuticAreaId) {
		this.therapeuticAreaId = therapeuticAreaId;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}

	public Integer getPercentage() {
		return percentage;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setUser(UserRegisterDto user) {
		this.user = user;
	}

	public UserRegisterDto getUser() {
		return user;
	}

	public void setTherapeuticArea(TherapeuticArea therapeuticArea) {
		this.therapeuticArea = therapeuticArea;
	}

	public TherapeuticArea getTherapeuticArea() {
		return therapeuticArea;
	}
}
